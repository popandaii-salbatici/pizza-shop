package ccir2482MV.validator;

import ccir2482MV.model.Payment;
import ccir2482MV.model.exceptions.ValidationExcepetion;

public interface PaymentValidator {
    void validate(Payment payment) throws ValidationExcepetion;
}
