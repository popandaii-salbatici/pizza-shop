package ccir2482MV.validator;

import ccir2482MV.model.Payment;
import ccir2482MV.model.exceptions.ValidationExcepetion;

public class AmountValidator implements PaymentValidator {

    @Override
    public void validate(Payment payment) throws ValidationExcepetion {
        if (payment.getAmount() < 0)
            throw new ValidationExcepetion("Amount < 0");
    }
}
