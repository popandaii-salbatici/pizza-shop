package ccir2482MV.validator;

import ccir2482MV.model.Payment;
import ccir2482MV.model.exceptions.ValidationExcepetion;

public class TableValidator implements PaymentValidator{

    @Override
    public void validate(Payment payment) throws ValidationExcepetion {
        if (payment.getTableNumber() < 1 || payment.getTableNumber() > 8)
            throw new ValidationExcepetion("Table < 0");
    }
}
