package ccir2482MV.service;

import ccir2482MV.model.MenuDataModel;
import ccir2482MV.model.Payment;
import ccir2482MV.model.PaymentType;
import ccir2482MV.repository.MenuRepository;
import ccir2482MV.repository.PaymentRepository;
import ccir2482MV.validator.AmountValidator;
import ccir2482MV.validator.PaymentValidator;
import ccir2482MV.validator.TableValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PizzaService {

    private MenuRepository menuRepo;
    private PaymentRepository payRepo;

    private List<PaymentValidator> paymentValidators = new ArrayList<>();

    public PizzaService(MenuRepository menuRepo, PaymentRepository payRepo){
        this.menuRepo=menuRepo;
        this.payRepo=payRepo;

        this.paymentValidators.add(new AmountValidator());
        this.paymentValidators.add(new TableValidator());
    }

    public List<MenuDataModel> getMenuData(){return menuRepo.getMenu();}

    public List<Payment> getPayments(){return payRepo.getAll(); }

    public void addPayment(int table, PaymentType type, double amount){
        Payment payment= new Payment(table, type, amount);

        paymentValidators.forEach(paymentValidator -> paymentValidator.validate(payment));

        payRepo.add(payment);
    }

    public double getTotalAmount(PaymentType type){
        double total=0.0f;

        List<Payment> l=getPayments();

        if (l.isEmpty()) return total;
        for (Payment p : l) {
            if (Objects.nonNull(p))
                if (p.getType().equals(type))
                    total+=p.getAmount();
        }
        return total;
    }

}
