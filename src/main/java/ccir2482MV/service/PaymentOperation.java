package ccir2482MV.service;

public interface PaymentOperation {
     void cardPayment();
     void cashPayment();
     void cancelPayment();
}
