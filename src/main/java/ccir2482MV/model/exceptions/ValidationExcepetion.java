package ccir2482MV.model.exceptions;

public class ValidationExcepetion extends RuntimeException {

    public ValidationExcepetion() {
        super();
    }

    public ValidationExcepetion(String message) {
        super(message);
    }

    public ValidationExcepetion(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationExcepetion(Throwable cause) {
        super(cause);
    }

    protected ValidationExcepetion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
