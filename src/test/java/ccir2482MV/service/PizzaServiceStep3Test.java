package ccir2482MV.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ccir2482MV.model.PaymentType;

import ccir2482MV.repository.MenuRepository;
import ccir2482MV.repository.PaymentRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class PizzaServiceStep3Test {
    private MenuRepository menuRepo;
    private PaymentRepository payRepo;
    private PizzaService pizzaService;

    @BeforeEach
    void setUp() {
        menuRepo = new MenuRepository();
        payRepo = new PaymentRepository();
        pizzaService = new PizzaService(menuRepo, payRepo);
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void addPayment_valid() throws Exception {
        int size = pizzaService.getPayments().size();

        pizzaService.addPayment(1, PaymentType.Cash, 100);

        assertEquals(size + 1, pizzaService.getPayments().size());
    }

    @Test
    void addPayment_invalidTable() {
        Exception exception = assertThrows(Exception.class, () -> pizzaService.addPayment(10, PaymentType.Cash, 100));
    }

    @Test
    void addPayment_invalidAmount() {
        Exception exception = assertThrows(Exception.class, () -> pizzaService.addPayment(1, PaymentType.Cash, -100));
    }

    @Test
    void getPayments_succeed() throws Exception {
        int size = pizzaService.getPayments().size();

        pizzaService.addPayment(1, PaymentType.Cash, 100);
        pizzaService.addPayment(2, PaymentType.Cash, 1001);

        assertEquals(size + 2, pizzaService.getPayments().size());
    }
}