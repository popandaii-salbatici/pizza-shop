package ccir2482MV.service;

import ccir2482MV.model.Payment;
import ccir2482MV.model.PaymentType;
import ccir2482MV.model.exceptions.ValidationExcepetion;
import ccir2482MV.repository.MenuRepository;
import ccir2482MV.repository.PaymentRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Tag;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PizzaServiceTest {

    @Mock
    private PaymentRepository mockPaymentRepository;

    @InjectMocks
    private PizzaService pizzaService;

    @Captor
    private ArgumentCaptor<Payment> paymentCaptor;

    @Before
    public void initialize() {
        MenuRepository menuRepository = new MenuRepository();
        mockPaymentRepository = Mockito.mock(PaymentRepository.class);
        pizzaService = new PizzaService(menuRepository, mockPaymentRepository);

        List<Payment> payments = new ArrayList<>();
        payments.add(new Payment(12, PaymentType.Card, 123));
        payments.add(new Payment(2, PaymentType.Cash, 45));
        payments.add(new Payment(1, PaymentType.Card, 55));

        when(mockPaymentRepository.getAll()).thenReturn(payments);
    }

    @Tag("ECP")
    @Test
    public void testSavePaymentWithInvalidType() {
        assertThrows(IllegalArgumentException.class , () -> pizzaService.addPayment(4, PaymentType.valueOf("WRONG"), 90.99));

        verify(mockPaymentRepository, never()).add(any(Payment.class));
    }

    @Tag("ECP")
    @Test
    public void testSavePaymentWithValidType() {
        Payment payment = new Payment(6, PaymentType.valueOf("Cash"), 900);

        pizzaService.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());

        verify(mockPaymentRepository).add(paymentCaptor.capture());
        Payment capturedPayment = paymentCaptor.getValue();

        assertNotNull(capturedPayment);
        assertEquals(capturedPayment.getAmount(), payment.getAmount(), 0);
        assertEquals(capturedPayment.getType(), payment.getType());
        assertEquals(capturedPayment.getTableNumber(), payment.getTableNumber());
    }

    @Tag("ECP")
    @Test
    public void testSavePaymentWithValidAmount() {
        Payment payment = new Payment(2, PaymentType.valueOf("Card"), 54.34);

        pizzaService.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());

        verify(mockPaymentRepository).add(paymentCaptor.capture());
        Payment capturedPayment = paymentCaptor.getValue();

        assertNotNull(capturedPayment);
        assertEquals(capturedPayment.getAmount(), payment.getAmount(), 0);
        assertEquals(capturedPayment.getType(), payment.getType());
        assertEquals(capturedPayment.getTableNumber(), payment.getTableNumber());
    }

    @Tag("BVA")
    @Test
    public void testSavePaymentWithInvalidAmount() {
        Payment payment = new Payment(1, PaymentType.Card, -0.01);

        assertThrows(ValidationExcepetion.class , () -> pizzaService.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount()));

        verify(mockPaymentRepository, never()).add(any(Payment.class));
    }

    @Tag("BVA")
    @Test
    public void testSavePaymentWithInvalidTable() {
        Payment payment = new Payment(0, PaymentType.Card, 10);

        assertThrows(ValidationExcepetion.class , () -> pizzaService.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount()));

        verify(mockPaymentRepository, never()).add(any(Payment.class));
    }

    @Tag("BVA")
    @Test
    public void testSavePaymentTableNumber1() {
        Payment payment = new Payment(1, PaymentType.Cash, 10);

        pizzaService.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());

        verify(mockPaymentRepository).add(paymentCaptor.capture());
        Payment capturedPayment = paymentCaptor.getValue();

        assertNotNull(capturedPayment);
        assertEquals(capturedPayment.getAmount(), payment.getAmount(), 0);
        assertEquals(capturedPayment.getType(), payment.getType());
        assertEquals(capturedPayment.getTableNumber(), payment.getTableNumber());
    }

    @Tag("BVA")
    @Test
    public void testSavePaymentAmountMinimum() {
        Payment payment = new Payment(1, PaymentType.Cash, 0.01);

        pizzaService.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());

        verify(mockPaymentRepository).add(paymentCaptor.capture());
        Payment capturedPayment = paymentCaptor.getValue();

        assertNotNull(capturedPayment);
        assertEquals(capturedPayment.getAmount(), payment.getAmount(), 0);
        assertEquals(capturedPayment.getType(), payment.getType());
        assertEquals(capturedPayment.getTableNumber(), payment.getTableNumber());
    }

    @Tag("F02_TC01")
    @Test
    public void testGetTotalPaymentWithEmptyList() {
        when(mockPaymentRepository.getAll()).thenReturn(Collections.emptyList());

        double amount = pizzaService.getTotalAmount(PaymentType.Cash);
        assertEquals(amount, 0, 0);
    }

    @Tag("F02_TC02")
    @Test
    public void testGetTotalPaymentWithNullObject() {
        List<Payment> objectList = new ArrayList<>();
        objectList.add(null);
        when(mockPaymentRepository.getAll()).thenReturn(objectList);

        double amount = pizzaService.getTotalAmount(PaymentType.Cash);
        assertEquals(amount, 0, 0);
    }

    @Tag("F02_TC03")
    @Test
    public void testGetTotalPaymentArrayHavingNoPaymentOfType() {
        when(mockPaymentRepository.getAll()).thenReturn(List.of(new Payment(8, PaymentType.Card, 10)));

        double amount = pizzaService.getTotalAmount(PaymentType.Cash);
        assertEquals(amount, 0, 0);
    }

    @Tag("F02_TC04")
    @Test
    public void testGetTotalPayment() {
        when(mockPaymentRepository.getAll()).thenReturn(List.of(new Payment(8, PaymentType.Cash, 10)));

        double amount = pizzaService.getTotalAmount(PaymentType.Cash);
        assertEquals(amount, 10, 0);
    }

    @Tag("F02_TC05")
    @Test
    public void testGetTotalPaymentWithMixedTypes() {
        when(mockPaymentRepository.getAll()).thenReturn(List.of(new Payment(8, PaymentType.Cash, 10),
                new Payment(2, PaymentType.Card, 10),
                new Payment(4, PaymentType.Card, 10)));

        double amount = pizzaService.getTotalAmount(PaymentType.Card);
        assertEquals(amount, 20, 0);
    }
}
